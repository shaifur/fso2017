/*
 * GmRssiTracking.h
 *
 *  Created on: Jul 7, 2017
 *      Author: wings
 */

#ifndef GMRSSITRACKING_H_
#define GMRSSITRACKING_H_

#include<string>
#include<sstream>
#include<exception>
#include<iostream>
#include<fstream>
#include<cstdlib>
#include<cstring>
#include<chrono>
#include<utility>
#include<map>
#include<tuple>
#include<new>
#include <cstddef>
#include<cmath>
#include <ncurses.h>

#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>


#include "pmd.h"
#include "usb-3100.h"

#define RSSI_SAMPLE_SIZE 4

#define LAST_COARSE_ALIGNMENT "last_alignment_max_rssi_voltages.txt"

#define RSSI_READ_DELAY_SAMPLE 2

#define ANGULAR_TOLERANCE_EQUIV_RSSI 200

#define BEAM_MOVEMENT_LATENCY_TEST_SIZE 5

#define PARAM_FILE_DELIM "="


using namespace std;

typedef pair<int, int> XY_VOLT_INDX;
typedef map<float, XY_VOLT_INDX > SORTED_RSSI_VALS;

enum Direction { CENTER, NORTH, SOUTH, EAST, WEST };

class GmRssiTracking {
protected:
	//variables
	hid_device* hid;
	uint8_t x_pin;
	uint8_t y_pin;
	float cur_x_volt;
	float cur_y_volt;
	//float aligned_x_volt;
	//float aligned_y_volt;
	string rssi_server_ip;
	int rssi_server_port;
	string training_filename;

	float training_x_volt_margin;
	float training_y_volt_margin;
	int num_x_grid;
	int num_y_grid;

	float *xvolts;
	float *yvolts;
	float **rssi_map;


	float max_rssi_xvolt;
	float max_rssi_yvolt;
	float max_rssi;
	SORTED_RSSI_VALS sorted_rssi_vals;
	int ns_grid_step;
	int ew_grid_step;
	float x_volt_per_grid;
	float y_volt_per_grid;
	int rssi_lookup_count; //# of items to lookup in each direction

	bool isTraining;
	bool isAligning;
	bool isCoarseAlignment;
	//std::fstream experiment_log_file;
	//methods
	void initDaq();
	void writeLog(string logmsg);
	float readRSSI();
	void setGMVoltage(float x_volt, float y_volt);
	float lookUpRSSI_Volts(int cur_x_grid, int cur_y_grid, Direction dir);
	float findNSEWCTupleDistance(tuple<int, int> grid_locs,
			                     tuple<float, float, float, float, float> nsewc_rssi_vals);
	float readRSSIWithDiscard(Direction dir);
	pair<float, float>  findGMCorrectionVoltage(float c_rssi, float n_rssi, float s_rssi, float e_rssi, float w_rssi);
	void correctGM();
	void parseConfigFile(const char *configFileName);
	void setParam(string key, string value);
	void loadTrainingData();
	void setGMToMaxRSSIPosition();
	void moveGMInGridStep(float x_grid_delta, float y_grid_delta);
	void gmrepeatibilityTest();
	/**
	 * Aligns the GM manually for max RSSI
	 * keys:
	 * LEFT_KEY/RIGHT_KEY: Left/Right;
	 * UP_KEY/DOWN_KEY: Up/Down;
	 * x: set delta-x;
	 * y: set delta-y;
	 * *:2x Current delta-xy,
	 * /: 1/2 current delta-xy;
	 * @param None
	 * @return None
	 */
	void coarseAlignGM();
	//float boost_readRSSI(string send_string="X_out");
	//void latencyTest(int delta);
	void train();
	void autoAlignExperiment();
	void stepAlignmentExperiment();
public:
	GmRssiTracking( const char *paramFilename);
	virtual ~GmRssiTracking();
	void runExperiment();
};
#endif /* GMRSSITRACKING_H_ */





