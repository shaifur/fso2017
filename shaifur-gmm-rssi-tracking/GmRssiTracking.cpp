/*
 * GmRssiTracking.cpp
 *
 *  Created on: Jul 7, 2017
 *      Author: wings
 */

#include "GmRssiTracking.h"

void GmRssiTracking::initDaq() {
	if ( hid_init()< 0) {
		cout<<"@hid_init(): Failed to Initialize the HIDAPI library! Check lib-hidapi driver installation\n";
		throw exception();

	}

	if ( (this->hid = hid_open(MCC_VID, USB3103_PID, NULL)  ) <= 0){
		cout<<"@hid_open(): DAQ USB-3103 Device Not Found! check USB connection\n";
		throw exception();
    }
	//configure pins
	usbAOutConfig_USB31XX(this->hid, this->x_pin, BP_10_00V);
    usbAOutConfig_USB31XX(this->hid, this->y_pin, BP_10_00V);
}



GmRssiTracking::GmRssiTracking(const char *paramFilename){
	this->parseConfigFile(paramFilename);
	//this->experiment_log_file.open("experiment_log.txt", ios::out);
    this->initDaq();
}


float GmRssiTracking::readRSSI() {
    //create socket
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    char buffer[64];
    float cur_rssi_val = -1;
    unsigned char buff[]="X_out";

    //send request to server
    struct sockaddr_in serv_addr;//began to binding the socket
    memset(&serv_addr, 0, sizeof(serv_addr));  //0zero for each byte
    serv_addr.sin_family = AF_INET;  //use IPv4
    //serv_addr.sin_addr.s_addr = inet_addr(this->rssi_server_ip.c_str());  //IP
    serv_addr.sin_addr.s_addr = inet_addr("192.168.1.177");
    //serv_addr.sin_port = htons(this->rssi_server_port);  //port
    serv_addr.sin_port = htons(8888);  //port
    connect(sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 5000;
    if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO,&tv,sizeof(tv)) < 0) {
        perror("Error");
    }


    //read back data
	//if(this->xout_sent == false){
	write(sock,buff,sizeof(buff));
			//this->xout_sent = true;
	//}

	int recvlen = recvfrom(sock, buffer, sizeof(buffer), 0, NULL, NULL);

	if (recvlen > 0) {
		buffer[recvlen] = 0;
		cur_rssi_val = atof(buffer);
	}

    close(sock);
    return cur_rssi_val;
}

void GmRssiTracking::setGMVoltage(float x_volt, float y_volt) {
	uint16_t x_pin_bits = 0;
	uint16_t y_pin_bits = 0;

	x_pin_bits = volts_USB31XX(BP_10_00V, x_volt);
	y_pin_bits = volts_USB31XX(BP_10_00V, y_volt);

	usbAOut_USB31XX(this->hid, this->x_pin, x_pin_bits, 0);
	usbAOut_USB31XX(this->hid, this->y_pin, y_pin_bits, 0);
}

void GmRssiTracking::coarseAlignGM() {
	float x_volt_delta = 0.0002;
	float y_volt_delta = 0.0002;
	float x_volt = this->cur_x_volt;
	float y_volt = this->cur_y_volt;
	this->setGMVoltage(x_volt, y_volt);

	float cur_rssi = 0.0;
	bool is_movement_key = false;
	int ch;
	float val;
	char str[80];
	initscr();			/* Start curses mode 		*/
	scrollok(stdscr,TRUE);
	raw();				/* Line buffering disabled	*/
	keypad(stdscr, TRUE);		/* We get F1, F2 etc..		*/
	noecho();			/* Don't echo() while we do getch */
	printw("Coarse Alignment: movement: LEFT/RIGHT UP/DOWN\nx/y:set delta-x/deltay\n* or /: 2x or 0.5x delta\nq: quit\n");
	while(true){
		is_movement_key = false;
    	ch = getch();
	    if(ch == KEY_UP){
	    	y_volt += y_volt_delta;
	    	if(y_volt > 10.0){
	    		printw("y-volt reached MAX! No change in y-mirror\n");
	    		y_volt = 10.0;
	    	}
	    	is_movement_key = true;
	    }
	    else if (ch == KEY_DOWN){
	    	y_volt -= y_volt_delta;
	    	if(y_volt < -10.0){
	    		printw("y-volt reached MIN! No change in y-mirror\n");
	    		y_volt = -10.0;
	    	}
	    	is_movement_key = true;
	    }
	    else if (ch == KEY_RIGHT){
	    	x_volt -= x_volt_delta;
	    	if(x_volt < -10.0){
	    		printw("x-volt reached MIN! No change in x-mirror\n");
	    		x_volt = -10.0;
	    	}
	    	is_movement_key = true;
	    }
	    else if (ch == KEY_LEFT){
	    	x_volt += x_volt_delta;
	    	if(x_volt > 10.0){
	    		printw("x-volt reached MAX! No change in x-mirror\n");
	    		x_volt = 10.0;
	    	}
	    	is_movement_key = true;
	    }
	    else if (ch == 's'){
	    	//--do nothing just show the rssi values---
	    	is_movement_key = true;
	    }
	    else if (ch == 'x'){
	    	printw("Enter delta-x:");
	    	echo();
	    	getstr(str);
	    	val = atof(str);
	    	if(val<= 0.0) printw("delta-x <= 0.0 !! try again by pressing x\n");
	    	else		  x_volt_delta = val;
			noecho();
	    }
	    else if (ch == 'y'){
	    	printw("Enter delta-y:");
	    	echo();
	    	getstr(str);
	    	val = atof(str);
	    	if(val<= 0.0) printw("delta-y <= 0.0 !! try again by pressing y\n");
	    	else		  y_volt_delta = val;
			noecho();
	    }
	    else if (ch == '/'){
	    	x_volt_delta /= 2.0;
	    	y_volt_delta /= 2.0;
	    	printw("x-delta: %f, y-delta: %f\n", x_volt_delta, y_volt_delta);
	    }
	    else if (ch == '*'){
	    	x_volt_delta *= 2.0;
	    	y_volt_delta *= 2.0;
	    	printw("x-delta: %f, y-delta: %f\n", x_volt_delta, y_volt_delta);
	    }

	    else if (ch =='q')			break;
	    else
	    	printw("Coarse Alignment:Movement: LEFT/RIGHT UP/DOWN\nx/y:set delta-x/deltay\n* or /: 2x or 0.5x delta\ns: Show RSSI \nq: quit");
	/*---------------END OF KEY-PRESS SWITCH------*/
	    if(is_movement_key){
			this->setGMVoltage(x_volt, y_volt);
			for(int i=0;i<RSSI_READ_DELAY_SAMPLE;++i)
				cur_rssi = this->readRSSI();
			cur_rssi = this->readRSSI();
			printw("x-volt:%f y-volt:%f RSSI: %f\n", x_volt, y_volt, cur_rssi);
	    }
	    refresh();			/* Print it on to the real screen */
	}
	endwin();
	//cout<<"End of Coarse Alignment\n";
	this->cur_x_volt = x_volt;
	this->cur_y_volt = y_volt;
	//cout<<"Current x-volt:"<<this->cur_x_volt<<" y-volt:"<<this->cur_y_volt<<endl;
	fstream configFile;
	configFile.open(LAST_COARSE_ALIGNMENT,ios::out);
	configFile<<this->cur_x_volt<<" "<<this->cur_y_volt<<endl;
	configFile.close();
	//this->aligned_x_volt = this->cur_x_volt;
	//this->aligned_y_volt = this->cur_y_volt;
}


GmRssiTracking::~GmRssiTracking() {
	// TODO Auto-generated destructor stub
}


void GmRssiTracking::train() {
	float x_volt_min =  this->cur_x_volt - this->training_x_volt_margin;
	float x_volt_max =  this->cur_x_volt + this->training_x_volt_margin;
	float x_volt_delta = (x_volt_max - x_volt_min)/this->num_x_grid;

	float y_volt_min =  this->cur_y_volt - this->training_y_volt_margin;
	float y_volt_max =  this->cur_y_volt + this->training_y_volt_margin;
	float y_volt_delta = (y_volt_max - y_volt_min)/this->num_y_grid;

	float rssi_vals[RSSI_SAMPLE_SIZE];
	float cur_rssi_val = 0.0;
	float avg_rssi_val = 0.0;
	int i;
	fstream training_data_File;
	fstream training_mesh_File;
	training_data_File.open((string(this->training_filename)+".all_vals.txt").c_str(),ios::out);
	training_mesh_File.open(this->training_filename.c_str(),ios::out);


	long long total_movement_time_ums = 0;
	int total_grids = (this->num_x_grid) * (this->num_y_grid);
	int grid_count = 0;
	cout<<"Saving training data for size "<<this->num_x_grid<<" X "<<this->num_y_grid<<" to file: "<<this->training_filename.c_str()<<endl;
	for(this->cur_x_volt = x_volt_min; this->cur_x_volt<=x_volt_max; this->cur_x_volt += x_volt_delta){
		for(this->cur_y_volt = y_volt_min; this->cur_y_volt <= y_volt_max; this->cur_y_volt += y_volt_delta){
			grid_count += 1;
			if(grid_count%1000==0){
				cout<<grid_count<<"/"<<total_grids<<" grids processed"<<endl;
			}
			auto start = std::chrono::high_resolution_clock::now();
			//------------------------------------------------------------//
			this->setGMVoltage(this->cur_x_volt, this->cur_y_volt);

			for(i=0;i<RSSI_READ_DELAY_SAMPLE;++i)
				cur_rssi_val = this->readRSSI();

			for(i=0;i<RSSI_SAMPLE_SIZE;++i){
				 cur_rssi_val = this->readRSSI();
				 rssi_vals[i] = cur_rssi_val;
				 avg_rssi_val += rssi_vals[i];
				 //write for data file
				 training_data_File<<this->cur_x_volt<<" "<<this->cur_y_volt<<" "<<rssi_vals[i]<<endl;
				//cout<<rssi_vals[i];
			}
			avg_rssi_val = avg_rssi_val/RSSI_SAMPLE_SIZE;
			//write for 3D mesh file
			training_mesh_File<<this->cur_x_volt<<" "<<this->cur_y_volt<<" "<<avg_rssi_val<<endl;
			//------------------------------------------------------------------------//
			auto elapsed = std::chrono::high_resolution_clock::now() - start;
			total_movement_time_ums+= std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
		}
		training_mesh_File<<endl;
	}
	training_mesh_File.close();
	training_data_File.close();
	long long per_movement_time_ums = total_movement_time_ums/(this->num_x_grid*this->num_y_grid);
	cout<<"Avg. time per movement (micro-second):"<<per_movement_time_ums<<endl;

}

void GmRssiTracking::loadTrainingData() {
	ifstream trainingFile(this->training_filename.c_str());
	if(!trainingFile.is_open()){
		cout<<"@loadTrainingData: Couldn't open training-file"<<endl;
		return;
	}
	this->xvolts = new (nothrow) float[this->num_x_grid];
	this->yvolts = new (nothrow) float[this->num_y_grid];
	this->rssi_map = new (nothrow) float* [this->num_x_grid];
	if(this->xvolts == nullptr || this->yvolts == nullptr){
		cout<<"@loadTrainingData: could not allocate memory fo xvolts/yvolts/rssi_vals* array!!";
		return;
	}

	for(int i = 0; i < this->num_x_grid; ++i){
		this->rssi_map[i] = new float[this->num_y_grid];
		if(this->rssi_map[i]  == nullptr){
			cout<<"@loadTrainingData: could not allocate memory for rssi_vals["<<i<<"] !!";
			return;
		}
	}

	float x_volt, y_volt, rssi;

	int x_volt_count = 0;
	int y_volt_count = 0;

	this->max_rssi = -1;
	this->max_rssi_xvolt = 0.0;
	this->max_rssi_yvolt = 0.0;

	while(!trainingFile.eof() ){
		trainingFile>>x_volt>>y_volt>>rssi;
		this->rssi_map[x_volt_count][y_volt_count] = rssi;
		this->xvolts[x_volt_count] = x_volt;
		this->yvolts[y_volt_count] = y_volt;
		this->sorted_rssi_vals[rssi] =  make_pair(x_volt_count, y_volt_count);

		if(this->max_rssi < rssi){
			this->max_rssi = rssi;
			this->max_rssi_xvolt = x_volt;
			this->max_rssi_yvolt = y_volt;
		}
		y_volt_count += 1;
		if(y_volt_count >= this->num_y_grid){
			y_volt_count = 0;
			x_volt_count += 1;
			if(x_volt_count>=this->num_x_grid)
				break;
		}
	}
	this->x_volt_per_grid = this->xvolts[1] - this->xvolts[0];
	this->y_volt_per_grid = this->yvolts[1] - this->yvolts[0];
	trainingFile.close();
	//cout<<"DEBUG:"<<this->x_volt_per_grid<<" "<<" "<<this->y_volt_per_grid<<endl;
	//cout<<this->max_rssi<<" "<<this->max_rssi_xvolt<<" "<<this->max_rssi_yvolt;
}

float GmRssiTracking::lookUpRSSI_Volts(int cur_x_grid, int cur_y_grid, Direction dir) {
	float rssi_val = -1;

	int new_x_grid, new_y_grid;
	if(dir == CENTER){
			new_x_grid = cur_x_grid;
			new_y_grid = cur_y_grid;
		}
	else if(dir == EAST){
		new_x_grid = cur_x_grid - this->ew_grid_step;
		if(new_x_grid < 0) new_x_grid = 0;
		new_y_grid = cur_y_grid;
	}
	else if(dir == WEST){
		new_x_grid = cur_x_grid + this->ew_grid_step;
		if(new_x_grid >= this->num_x_grid) new_x_grid = this->num_x_grid - 1;
		new_y_grid = cur_y_grid;
	}
	else if(dir == NORTH){
		new_x_grid = cur_x_grid;
		new_y_grid = cur_y_grid + this->ns_grid_step;
		if(new_y_grid >= this->num_y_grid) new_y_grid = this->num_y_grid -1;
	}
	else if(dir == SOUTH){
		new_x_grid = cur_x_grid;
		new_y_grid = cur_y_grid - this->ns_grid_step;
		if(new_y_grid < 0) new_y_grid = 0;
	}
	rssi_val = this->rssi_map[new_x_grid][new_y_grid];
	return rssi_val;
}

float GmRssiTracking::findNSEWCTupleDistance(tuple<int, int> grid_locs,
		                                     tuple<float, float, float, float, float> cnsew_rssi_vals) {

	float sum_distance = 0;
	//--CENTER--//
	sum_distance += fabs( get<CENTER>(cnsew_rssi_vals) -  this->lookUpRSSI_Volts(get<0>(grid_locs),
			  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	     get<1>(grid_locs),
			  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	   CENTER)  );
	//--EAST--//
	sum_distance += fabs( get<EAST>(cnsew_rssi_vals) -  this->lookUpRSSI_Volts(get<0>(grid_locs),
			  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	   get<1>(grid_locs),
			  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	   EAST)  );
	//--WEST--//
	sum_distance += fabs( get<WEST>(cnsew_rssi_vals) -  this->lookUpRSSI_Volts(get<0>(grid_locs),
			  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	   get<1>(grid_locs),
			  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	   WEST)  );
	//--NORTH--//
	sum_distance += fabs( get<NORTH>(cnsew_rssi_vals) -  this->lookUpRSSI_Volts(get<0>(grid_locs),
			  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	    get<1>(grid_locs),
			  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	   NORTH)  );
	//--SOUTH--//
	sum_distance += fabs( get<SOUTH>(cnsew_rssi_vals) -  this->lookUpRSSI_Volts(get<0>(grid_locs),
			  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	    get<1>(grid_locs),
			  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	   SOUTH)  );
	return sum_distance;
}

pair<float, float> GmRssiTracking::findGMCorrectionVoltage(float c_rssi, float n_rssi, float s_rssi, float e_rssi, float w_rssi) {
	/*
	 * Read the closest match in the rssi value, read the first k values before and after and find the
	 * x_volt, y_volt with minimum 5-tuple distance, then apply the appropriate voltoage to GM
	 */
	SORTED_RSSI_VALS::iterator srv_iter = this->sorted_rssi_vals.lower_bound(c_rssi);;
	SORTED_RSSI_VALS::reverse_iterator r_srv_iter(srv_iter);
	++r_srv_iter;

	int iter_count;

	int min_d_xindx = -1;
	int min_d_yindx = -1;
	float min_dist = 100000.0;

	int cur_x_indx = -1;
	int cur_y_indx = -1;
	float cur_dist = 100000.0;

	iter_count = 0;
	for(  ;srv_iter != this->sorted_rssi_vals.end();  ++srv_iter){
		cur_x_indx = srv_iter->second.first;
		cur_y_indx = srv_iter->second.second;
		cur_dist = this->findNSEWCTupleDistance( make_tuple(cur_x_indx, cur_y_indx),
												 make_tuple(c_rssi, n_rssi, s_rssi, e_rssi, w_rssi)
												);
		if(cur_dist<min_dist){
			min_dist = cur_dist;
			min_d_xindx = cur_x_indx;
			min_d_yindx = cur_y_indx;
		}
		iter_count += 1;
		if(iter_count >= this->rssi_lookup_count) break;
	}

	iter_count = 0;
	for(  ;r_srv_iter != this->sorted_rssi_vals.rend();  ++r_srv_iter){
		cur_x_indx = r_srv_iter->second.first;
		cur_y_indx = r_srv_iter->second.second;
		cur_dist = this->findNSEWCTupleDistance( make_tuple(cur_x_indx, cur_y_indx),
												 make_tuple(c_rssi, n_rssi, s_rssi, e_rssi, w_rssi)
												);
		if(cur_dist<min_dist){
			min_dist = cur_dist;
			min_d_xindx = cur_x_indx;
			min_d_yindx = cur_y_indx;
		}
		iter_count += 1;
		if(iter_count >= this->rssi_lookup_count) break;
	}
	///---find GM correction  voltage---//
	float delta_x_volt = this->xvolts[min_d_xindx] -  this->max_rssi_xvolt;
	float delta_y_volt = this->yvolts[min_d_yindx] -  this->max_rssi_yvolt;
	stringstream logmsg;
	logmsg<<"lookup min-d: "<<min_dist<<", x,y-grids: "<<min_d_xindx<<", "<<min_d_yindx<<" x,y-volts: "
			                 <<this->xvolts[min_d_xindx]<<", "<<this->yvolts[min_d_yindx];
	this->writeLog(logmsg.str());
	//cout<<"DEBUG:"<<this->xvolts[min_d_xindx]<<"  vs  "<<this->max_rssi_xvolt<<endl;
	//cout<<"DEBUG:"<<this->yvolts[min_d_yindx]<<"  vs  "<<this->max_rssi_yvolt<<endl;
	return make_pair(delta_x_volt, delta_y_volt);
}

float GmRssiTracking::readRSSIWithDiscard(Direction dir) {
	float rssi_val = -1;
	float new_x_volt;
	float new_y_volt;
	float delta_x_volt = (this->ew_grid_step) * (this->x_volt_per_grid);
	float delta_y_volt = (this->ns_grid_step) * (this->y_volt_per_grid);

	//move the GM
	if(dir == CENTER){
		new_x_volt = this->cur_x_volt;
		new_y_volt = this->cur_y_volt;
	}
	else if(dir == EAST){
		new_x_volt = this->cur_x_volt - delta_x_volt;
		new_y_volt = this->cur_y_volt;
	}
	else if(dir == WEST){
		new_x_volt = this->cur_x_volt + delta_x_volt;
		new_y_volt = this->cur_y_volt;
	}
	else if(dir == NORTH){
		new_x_volt = this->cur_x_volt;
		new_y_volt = this->cur_y_volt  + delta_y_volt;
	}
	else if(dir == SOUTH){
		new_x_volt = this->cur_x_volt;
		new_y_volt = this->cur_y_volt - delta_y_volt;
	}
	this->setGMVoltage(new_x_volt, new_y_volt);
	for(int i=0;i<RSSI_READ_DELAY_SAMPLE;++i)
		rssi_val = this->readRSSI();
	rssi_val = this->readRSSI();
	this->setGMVoltage(this->cur_x_volt, this->cur_y_volt);
	return rssi_val;
}

void GmRssiTracking::correctGM() {
	//move GM center, north, south, east, west and read the current RSSIs
	float c_rssi =  this->readRSSIWithDiscard(CENTER);
	float n_rssi =  this->readRSSIWithDiscard(NORTH);
	float s_rssi =  this->readRSSIWithDiscard(SOUTH);
	float e_rssi =  this->readRSSIWithDiscard(EAST);
	float w_rssi =  this->readRSSIWithDiscard(WEST);
	stringstream logmsg;
	logmsg<<"GM C, N, S, E, W volts: "<<c_rssi<<", "<<n_rssi<<", "<<", "<<s_rssi<<", "<<e_rssi<<", "<<w_rssi;
	this->writeLog(logmsg.str());
	//call find gm correction  voltage
	auto start = std::chrono::high_resolution_clock::now(); //<--timer started
	float delta_x_volt, delta_y_volt;
	tie(delta_x_volt, delta_y_volt) = findGMCorrectionVoltage(c_rssi, n_rssi, s_rssi, e_rssi, w_rssi);
	//apply the voltage
	auto elapsed = std::chrono::high_resolution_clock::now() - start; //<--timer ended
	long long total_movement_time_ums = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count(); //<--elapsed time
	float etime = total_movement_time_ums/ 1000;
	this->cur_x_volt -= delta_x_volt;
	this->cur_y_volt -= delta_y_volt;
	this->setGMVoltage(this->cur_x_volt, this->cur_y_volt);
	logmsg<<"GM corrected by "<<delta_x_volt<<" , "<<delta_y_volt<<" volts, etime:  "<<etime;
	this->writeLog(logmsg.str());
}


void GmRssiTracking::setGMToMaxRSSIPosition() {
	this->cur_x_volt = this->max_rssi_xvolt;
	this->cur_y_volt = this->max_rssi_yvolt;
	this->setGMVoltage(this->cur_x_volt, this->cur_y_volt);
}

void GmRssiTracking::setParam(string key, string value) {
	//cout<<"value:"<<value<<endl;
	if(key.find("DAQ_XVOLT_PIN") != string::npos){
		this->x_pin = (uint8_t) stoi(value);
		//cout<<"DEBUG: x_pin:"<<(unsigned) this->x_pin<<endl;
	}
	else if(key.find("DAQ_YVOLT_PIN") != string::npos){
		this->y_pin = (uint8_t) stoi(value);
		//cout<<"DEBUG: y_pin:"<<(unsigned) this->y_pin<<endl;
	}
	else if(key.find("GM_INITIAL_X_VOLT") != string::npos){
		this->cur_x_volt = stof(value);
		//cout<<"DEBUG: cur_x_volt:"<<this->cur_x_volt<<endl;
	}
	else if(key.find("GM_INITIAL_Y_VOLT") != string::npos){
		this->cur_y_volt = stof(value);
		//cout<<"DEBUG: cur_y_volt:"<<this->cur_y_volt<<endl;
	}
	else if(key.find("ARDUINO_IP_ADDRESS") != string::npos){
		this->rssi_server_ip = string(value);
		//cout<<"DEBUG: rssi_server_ip:"<<this->rssi_server_ip<<endl;
	}
	else if(key.find("ARDUINO_PORT_NO") != string::npos){
		this->rssi_server_port = stoi(value);
		//cout<<"DEBUG: rssi_server_port:"<<this->rssi_server_port<<endl;
	}
	else if(key.find("TRAINING_FILENAME") != string::npos){
		this->training_filename = string(value);
		//cout<<"DEBUG: training_filename:"<<this->training_filename.c_str()<<endl;
	}
	else if(key.find("TRAINING_GM_X_VOLT_MARGIN") != string::npos){
		this->training_x_volt_margin =  stof(value);
		//cout<<"DEBUG: training_x_volt_margin:"<<this->training_x_volt_margin<<endl;
	}
	else if(key.find("TRAINING_GM_Y_VOLT_MARGIN") != string::npos){
		this->training_y_volt_margin  = stof(value);
		//cout<<"DEBUG: training_y_volt_margin:"<<this->training_y_volt_margin<<endl;
	}
	else if(key.find("TRAINING_X_GRID_SIZE") != string::npos){
		this->num_x_grid = stoi(value);
		//cout<<"DEBUG: num_x_grid:"<<this->num_x_grid<<endl;
	}
	else if(key.find("TRAINING_Y_GRID_SIZE") != string::npos){
		this->num_y_grid = stoi(value);
		//cout<<"DEBUG: num_x_grid:"<<this->num_x_grid<<endl;
	}
	else if(key.find("ALIGNMENT_RSSI_LOOKUP_COUNT") != string::npos){
		this->rssi_lookup_count = stoi(value);
		//cout<<"DEBUG: rssi_lookup_count:"<<this->rssi_lookup_count<<endl;
	}
	else if(key.find("ALIGNMENT_LOOKUP_NORTH_SOUTH_GRID") != string::npos){
		this->ns_grid_step = stoi(value);
		//cout<<"DEBUG: ns_grid_step:"<<this->ns_grid_step<<endl;
	}
	else if(key.find("ALIGNMENT_LOOKUP_EAST_WEST_GRID") != string::npos){
		this->ew_grid_step = stoi(value);
		//cout<<"DEBUG: ew_grid_step:"<<this->ew_grid_step<<endl;
	}
	else if(key.find("RUN_TRAINING") != string::npos){
		if(value.find("y") != string::npos) this->isTraining = true;
		else this->isTraining = false;
		//cout<<"DEBUG: isTraining:"<<this->isTraining<<endl;
	}
	else if(key.find("RUN_AUTOALIGNMENT") != string::npos){
		if(value.find("y") != string::npos) this->isAligning = true;
		else this->isAligning = false;
		//cout<<"DEBUG: isAligning:"<<this->isAligning<<endl;
	}
	else if(key.find("RUN_COARSE_ALIGNMENT") != string::npos){
		if(value.find("y") != string::npos) this->isCoarseAlignment = true;
		else this->isCoarseAlignment = false;

	}
}

void GmRssiTracking::parseConfigFile(const char *configFileName) {
	   ifstream configFile(configFileName);
		if(!configFile.is_open()){
			cout<<"@parseConfigFile: Couldn't open config-file"<<endl;
			return;
		}
		string line;
		string key;
		string value;
		unsigned int tokenPos;
		while( getline(configFile, line) )
		{
			if(line[0]=='#') continue;
			tokenPos = line.find(PARAM_FILE_DELIM);
			if( tokenPos == string::npos) continue;
			key = line.substr(0, tokenPos);
			value = line.substr(tokenPos+1);
			//cout<<key<<":"<<value<<endl;
			this->setParam(key, value);
		}
}

void GmRssiTracking::autoAlignExperiment() {
	//load training file
	this->loadTrainingData();
	//set lookup params
	this->setGMToMaxRSSIPosition();

	cout<<"Running Auto-alignment test, press Ctrl+Z to stop.."<<endl;
	float cur_rssi; //, prev_rssi, init_rssi;
	cur_rssi =  this->readRSSIWithDiscard(CENTER);
	//init_rssi = cur_rssi;
	//prev_rssi = cur_rssi;
	while(true){
		//read rssi, check if it has changed more than 10%, if so trigger GM autoalignment
		cur_rssi = this->readRSSIWithDiscard(CENTER);
		stringstream logmsg;
		logmsg<<"rssi: "<<cur_rssi;
		this->writeLog(logmsg.str());
		//cout<<"Current RSSI: "<<cur_rssi<<" / "<<init_rssi<<" / "<<this->max_rssi<<endl;
		cout<<"Current RSSI: "<<cur_rssi<<", cur xvolt, yvolt: "<<this->cur_x_volt<<", "<<this->cur_y_volt<<endl;
		if(fabs(   (cur_rssi - this->max_rssi)  ) > ANGULAR_TOLERANCE_EQUIV_RSSI){ //TODO: embed this value in the param file
			this->correctGM();
			cout<<"GM corrected.."<<endl;
		}
	}
}

void GmRssiTracking::stepAlignmentExperiment() {
	//load training file
	this->loadTrainingData();
	//set lookup params
	this->setGMToMaxRSSIPosition();

	cout<<"Running Step-alignment test in all directions .."<<endl;
	cout<<"RSSI Output format: current_rssi/ rssi_read_at_max_training_location / max_training_rssi "<<endl;
	float cur_rssi, init_rssi;
	cur_rssi =  this->readRSSIWithDiscard(CENTER);
	init_rssi = cur_rssi;
	//prev_rssi = cur_rssi;
	float grid_size =  75.25;
	float x_grid_delta[] = {grid_size,  grid_size, -grid_size, -grid_size};
	float y_grid_delta[] = {grid_size, -grid_size,  grid_size, -grid_size};
	for(int i=0; i<4;++i){
		//apply change
		cout<<"x_grid, y_grid movement: "<<x_grid_delta[i]<<" , "<<y_grid_delta[i]<<endl;
		this->moveGMInGridStep(x_grid_delta[i], y_grid_delta[i]);
		cur_rssi = this->readRSSIWithDiscard(CENTER);
		cout<<"\tRSSI Before Correction: "<<cur_rssi<<" / "<<init_rssi<<" / "<<this->max_rssi<<endl;
		//********************************----
		this->correctGM();
		//********************************----
		cur_rssi = this->readRSSIWithDiscard(CENTER);
		cout<<"\tRSSI After Correction: "<<cur_rssi<<" / "<<init_rssi<<" / "<<this->max_rssi<<endl;
		//---------now restore to max position----------------------------------------------------//
		this->setGMToMaxRSSIPosition();
		cur_rssi = this->readRSSIWithDiscard(CENTER);
		//cur_rssi = this->readRSSIWithDiscard(CENTER);
		//cout<<"\tCurrent RSSI: "<<cur_rssi<<" / "<<init_rssi<<" / "<<this->max_rssi<<endl;
	}
}

void GmRssiTracking::moveGMInGridStep(float x_grid_delta, float y_grid_delta) {
	this->cur_x_volt += x_grid_delta * this->x_volt_per_grid;
	this->cur_y_volt += y_grid_delta * this->y_volt_per_grid;
	this->setGMVoltage(this->cur_x_volt, this->cur_y_volt);
}

void GmRssiTracking::gmrepeatibilityTest() {
	//load training file
	this->loadTrainingData();
	//set lookup params
	this->setGMToMaxRSSIPosition();

	cout<<"Running GM repeatability test in all directions .."<<endl;
	cout<<"RSSI Output format: current_rssi "<<endl;
	float cur_rssi;
	cur_rssi =  this->readRSSIWithDiscard(CENTER);
	//prev_rssi = cur_rssi;
	float grid_size =  30.25;
	float x_grid_delta[] = {grid_size,  grid_size, -grid_size, -grid_size};
	float y_grid_delta[] = {grid_size, -grid_size,  grid_size, -grid_size};


	for(int i=0; i<4;++i){
		//cout<<"x_grid, y_grid movement: "<<x_grid_delta[i]<<" , "<<y_grid_delta[i]<<endl;
		cout<<"test#"<<endl;
		this->setGMToMaxRSSIPosition();
		cur_rssi = this->readRSSIWithDiscard(CENTER);
		cout<<"\tRSSI Before Moving>> "<<cur_rssi<<endl;

		this->moveGMInGridStep(x_grid_delta[i], y_grid_delta[i]);
		cur_rssi = this->readRSSIWithDiscard(CENTER);
		cout<<"\tRSSI After Moving: "<<cur_rssi<<endl;

		this->setGMToMaxRSSIPosition();
		cur_rssi = this->readRSSIWithDiscard(CENTER);
		cout<<"\tRSSI After Restoring>> "<<cur_rssi<<endl;
	}
}

void GmRssiTracking::writeLog(string logmsg) {
	fstream exp_log_file;
	exp_log_file.open("experiment_log.txt", fstream::in | fstream::out| fstream::app);
	if(!exp_log_file.is_open()){
		cout<<"@writeLog: Couldn't open log-file"<<endl;
		return;
	}
	exp_log_file<<logmsg.c_str()<<endl;
	exp_log_file.close();

}

void GmRssiTracking::runExperiment() {
	if(this->isCoarseAlignment){
		this->coarseAlignGM();
	}
	if(this->isTraining){
		this->train();
	}
	if(this->isAligning){
		this->autoAlignExperiment();
		//this->stepAlignmentExperiment();
		//this->gmrepeatibilityTest();
	}
}











