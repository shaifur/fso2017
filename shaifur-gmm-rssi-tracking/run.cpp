/*
 * run.cpp
 *
 *  Created on: Jul 7, 2017
 *      Author: wings
 */
#include<iostream>
#include<exception>


#include "GmRssiTracking.h"


using namespace std;



int main(int argc, char **argv ){

	const char *configFileName = "param.txt";
	try{
		GmRssiTracking gm_rssi = GmRssiTracking(configFileName);
		gm_rssi.runExperiment();
	}catch(const exception &e){
		 return 1;
	}
}


