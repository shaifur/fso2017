To run GM-RSSI-Auto alignment experiment in a Linux (tested on Ubuntu 16.04) machine
1. run install.sh to install drivers and required packages
2. run 'make' in the current directory
3. run gmrssi as executable

For any trouble-shooting, contact: mdsrahman@cs.stonybrook.edu